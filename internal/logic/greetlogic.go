package logic

import (
	"context"

	"gitee.com/yang-sunhang/weikang_rpc/internal/svc"
	"gitee.com/yang-sunhang/weikang_rpc/weikangrpc"

	"github.com/zeromicro/go-zero/core/logx"
)

type GreetLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGreetLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GreetLogic {
	return &GreetLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GreetLogic) Greet(in *weikangrpc.StreamReq) (*weikangrpc.StreamResp, error) {
	// todo: add your logic here and delete this line

	return &weikangrpc.StreamResp{}, nil
}
