package main

import (
	"flag"
	"fmt"

	"gitee.com/yang-sunhang/weikang_rpc/internal/config"
	"gitee.com/yang-sunhang/weikang_rpc/internal/server"
	"gitee.com/yang-sunhang/weikang_rpc/internal/svc"
	"gitee.com/yang-sunhang/weikang_rpc/weikangrpc"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var configFile = flag.String("f", "etc/weikang.yaml", "the config file")

func main() {
	flag.Parse()

	var c config.Config
	conf.MustLoad(*configFile, &c)
	ctx := svc.NewServiceContext(c)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		weikangrpc.RegisterWeikangServer(grpcServer, server.NewWeikangServer(ctx))

		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})
	defer s.Stop()

	fmt.Printf("Starting rpc server at %s...\n", c.ListenOn)
	s.Start()
}
